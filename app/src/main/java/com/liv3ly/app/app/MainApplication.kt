package com.liv3ly.app.app

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.liv3ly.app.BuildConfig
import com.liv3ly.app.di.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication: Application() , LifecycleObserver {

    private object Holder {
        lateinit var INSTANCE: MainApplication
    }

    companion object {
        @JvmStatic
        fun getInstance(): MainApplication {
            return Holder.INSTANCE
        }
    }

    override fun onCreate() {
        super.onCreate()
        Holder.INSTANCE = this
        initKoin()
        initTimber()
        setupLifecycleListener()
    }

    private fun initKoin() {
        // start Koin context
        startKoin {
            // Use Android Context from MainApplication
            androidContext(this@MainApplication)
            androidLogger()
            modules(
                listOf(
                    splashModule,
                    appModule,
                    networkModule,
                    localModule,
                    dataModule,
                    mainModule,
                    shapeModule
                )
            )
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun setupLifecycleListener() {
        ProcessLifecycleOwner.get().lifecycle
            .addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        Timber.d("Returning to foreground…")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        Timber.d("Moving to background…")
    }
}