package com.vatnow.app.constants

object Constants {

    const val DEFAULT_FIRST_PAGE = 1
    const val DEFAULT_NUM_VISIBLE_THRESHOLD = 10
    const val DEFAULT_ITEM_PER_PAGE = 10
    const val THRESHOLD_CLICK_TIME = 1000
    const val MIN_PASSWORD_LENGTH = 8

    const val DATABASE_NAME = "com.vatnow.app.DATABASE"
    const val HTTP_NOT_FOUND = 404

    const val FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    const val FORMAT_DATE_DISPLAY = "dd/MM/yyyy"
    const val FORMAT_DATE_TIME_DISPLAY = "dd/MM/yyyy - HH:mm"
    const val FORMAT_SECTION_DATE = "MMMM/yyyy"

    const val DIALOG_DISMISS_INTERVAL = 1500L

    const val PAGE_INIT_ELEMENTS = 1
    const val PAGE_MAX_ELEMENTS = 15

}