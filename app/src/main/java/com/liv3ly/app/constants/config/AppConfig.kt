package com.liv3ly.app.constants.config

import android.text.format.DateUtils


object AppConfig {

    // Retrofit
    const val TIMEOUT_READ_MILLIS = DateUtils.MINUTE_IN_MILLIS
    const val TIMEOUT_CONNECT_MILLIS = DateUtils.MINUTE_IN_MILLIS
    const val TIMEOUT_WRITE_MILLIS = DateUtils.MINUTE_IN_MILLIS

    // Amazon S3
    const val MAX_IMAGE_FILE_SIZE = 256 * 1024L // kB
    const val MAX_IMAGE_MAX_SIZE = 1024 // px
}