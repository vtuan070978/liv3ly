package com.vatnow.app.constants.preference

object PrefsKey {
    const val FIRST_RUN = "APP_FIRST_RUN"
    const val TOKEN = "TOKEN"
    const val CURRENT_USER = "CURRENT_USER"
    const val CURRENT_USER_ID = "CURRENT_USER_ID"
    const val SETTING_CONFIG = "SETTING_CONFIG"
    const val FCM_DEVICE_TOKEN = "FCM_DEVICE_TOKEN"
    const val DEVICE_ID = "DEVICE_ID"
    const val MUST_UPDATE_PASSWORD = "MUST_UPDATE_PASSWORD"
    const val APP_SET_LANGUAGE = "APP_SET_LANGUAGE"
    const val SETTING_LANGUAGE = "SETTING_LANGUAGE"

    //key for prefs and firebase remote config
    const val ENABLE_FEATURE_PACKAGE = "enable_feature_package"
}