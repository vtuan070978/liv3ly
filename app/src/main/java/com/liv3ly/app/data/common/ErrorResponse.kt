package com.liv3ly.app.data.common

data class ErrorResponse(
    val statusCode: Int? = 0,
    val message: String? = null
)