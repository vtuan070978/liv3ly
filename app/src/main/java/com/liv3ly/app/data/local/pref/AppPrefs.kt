package com.liv3ly.app.data.local.pref

import android.content.Context
import androidx.core.content.edit
import com.vatnow.app.constants.preference.PrefsKey

class AppPrefs(
    private val context: Context
) : PrefHelper {

    private val sharedPreferences = context.getSharedPreferences(
        context.packageName,
        Context.MODE_PRIVATE
    )

    override fun getUserToken(): String? {
        return sharedPreferences.getString(PrefsKey.TOKEN, "")
    }

    override fun saveUserToken(token: String?) {
        sharedPreferences.edit {
            putString(PrefsKey.TOKEN, token ?: "")
        }
    }

    override fun setFirstRun(isFirstRun: Boolean) {
        sharedPreferences.edit {
            putBoolean(PrefsKey.FIRST_RUN, isFirstRun)
        }
    }

    override fun isFirstRun(): Boolean {
        return sharedPreferences.getBoolean(PrefsKey.FIRST_RUN, true);
    }

    override fun remove(key: String) {
        sharedPreferences.edit {
            remove(key)
        }
    }

    override fun clear() {
        sharedPreferences.edit { clear() }
    }

}