package com.liv3ly.app.data.local.pref

interface PrefHelper {

    fun getUserToken() : String?

    fun isFirstRun(): Boolean

    fun remove(key: String)

    fun clear()

    fun saveUserToken(token: String?)

    fun setFirstRun(isFirstRun : Boolean)

}