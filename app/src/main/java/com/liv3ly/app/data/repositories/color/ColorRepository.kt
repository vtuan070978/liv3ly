package com.liv3ly.app.data.repositories.color

import com.liv3ly.app.data.repositories.color.model.ColorModel
import io.reactivex.Observable
import io.reactivex.Single

interface ColorRepository {
    fun getColour(): Single<List<ColorModel>>
}