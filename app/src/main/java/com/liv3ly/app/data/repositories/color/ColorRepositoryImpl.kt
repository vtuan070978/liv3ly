package com.liv3ly.app.data.repositories.color

import com.liv3ly.app.data.repositories.color.model.ColorModel
import io.reactivex.Single

class ColorRepositoryImpl(val service: ColorService): ColorRepository {

    override fun getColour(): Single<List<ColorModel>> {
        return service.getColor()
    }
}