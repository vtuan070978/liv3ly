package com.liv3ly.app.data.repositories.color

import com.liv3ly.app.data.repositories.color.model.ColorModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET

interface ColorService {

    @GET("/api/colors/random?format=json")
    fun getColor(): Single<List<ColorModel>>
}