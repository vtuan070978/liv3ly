package com.liv3ly.app.data.repositories.color.model

data class ColorModel(
    val id: Long?= null,
    val title: String?= null,
    val userName: String?= null,
    val numViews: Int?= null,
    val numVotes: Int?= null,
    val numComments: Int?= null,
    val numHearts: Int?= null,
    val rank: Int?= null,
    val hex: String?= null,
    val rgb: ColourRGB?= null,
    val hsv: ColourHSV?= null,
    val description: String?= null,
    val url: String?= null,
    val imageUrl: String?= null,
    val badgeUrl: String?= null,
    val apiUrl: String?= null
)
