package com.liv3ly.app.data.repositories.color.model

data class ColourHSV(
    val hue:Int? = null,
    val saturation:Int? = null,
    val value:Int? = null
)
