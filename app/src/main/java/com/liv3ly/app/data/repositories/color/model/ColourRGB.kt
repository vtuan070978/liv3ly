package com.liv3ly.app.data.repositories.color.model

data class ColourRGB(
    val red:Int? = null,
    val green:Int? = null,
    val blue:Int? = null
)
