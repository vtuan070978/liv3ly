package com.liv3ly.app.data.repositories.pattern

import com.liv3ly.app.data.repositories.pattern.model.PatternModel
import io.reactivex.Observable
import io.reactivex.Single

interface PatternRepository {

    fun getPatterns(): Single<List<PatternModel>>

}