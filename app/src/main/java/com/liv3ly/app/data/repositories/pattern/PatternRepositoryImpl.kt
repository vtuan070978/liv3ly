package com.liv3ly.app.data.repositories.pattern

import com.liv3ly.app.data.repositories.pattern.model.PatternModel
import io.reactivex.Observable
import io.reactivex.Single

class PatternRepositoryImpl(private val patternService: PatternService): PatternRepository {

    override fun getPatterns(): Single<List<PatternModel>> {
        return patternService.getPatterns()
    }
}