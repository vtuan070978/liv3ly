package com.liv3ly.app.data.repositories.pattern

import com.liv3ly.app.data.repositories.pattern.model.PatternModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET

interface PatternService {

    @GET("/api/patterns/random?format=json")
    fun getPatterns(): Single<List<PatternModel>>
}