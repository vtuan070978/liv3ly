package com.liv3ly.app.data.repositories.pattern.model

data class PatternModel(
    val id: Long?= null,
    val title: String?= null,
    val userName: String?= null,
    val numViews: Int?= null,
    val numVotes: Int?= null,
    val numComments: Int?= null,
    val numHearts: Int?= null,
    val colors: List<String>?= null,
    val description: String?= null,
    val url: String?= null,
    val imageUrl: String?= null,
    val badgeUrl: String?= null,
    val apiUrl: String?= null
)
