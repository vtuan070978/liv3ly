package com.liv3ly.app.di

import com.liv3ly.app.ui.utils.StringUtils
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {
    single { StringUtils(androidContext()) }
}