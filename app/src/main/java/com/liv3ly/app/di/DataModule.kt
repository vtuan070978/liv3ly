package com.liv3ly.app.di

import com.liv3ly.app.data.repositories.color.ColorRepository
import com.liv3ly.app.data.repositories.color.ColorRepositoryImpl
import com.liv3ly.app.data.repositories.pattern.PatternRepository
import com.liv3ly.app.data.repositories.pattern.PatternRepositoryImpl
import org.koin.dsl.module

val dataModule = module {
    single<ColorRepository> { ColorRepositoryImpl(get()) }
    single<PatternRepository> { PatternRepositoryImpl(get()) }
}