package com.liv3ly.app.di

import com.liv3ly.app.ui.screens.MainViewModel
import com.liv3ly.app.ui.screens.splash.SplashScreenViewModel
import com.liv3ly.app.ui.screens.tabbar.ShapeViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module
import org.koin.experimental.builder.factory

val splashModule = module {
    viewModel<SplashScreenViewModel>()
}
val mainModule = module {
    viewModel<MainViewModel>()
}

val shapeModule = module {
    viewModel<ShapeViewModel>()
}
