package com.liv3ly.app.di

import com.liv3ly.app.data.local.pref.AppPrefs
import com.liv3ly.app.data.local.pref.PrefHelper
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val localModule = module {
    single<PrefHelper> { AppPrefs(androidContext()) }
}