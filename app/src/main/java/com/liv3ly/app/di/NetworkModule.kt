package com.liv3ly.app.di

import android.annotation.SuppressLint
import com.liv3ly.app.BuildConfig
import com.liv3ly.app.constants.config.AppConfig
import com.liv3ly.app.data.repositories.color.ColorService
import com.liv3ly.app.data.repositories.pattern.PatternService
import com.liv3ly.app.retrofit.AuthenticatorInterceptor
import com.liv3ly.app.retrofit.NullOrEmptyConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.koin.experimental.builder.factory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*


val networkModule = module {
    factory<AuthenticatorInterceptor>()
    factory { getLoggingInterceptor() }
    factory(named("auth")) { getOkHttpClientBuilder(get(), get(), true) }
    factory(named("no_auth")) { getOkHttpClientBuilder(get(), get(), false) }
    single { getRetrofit(get(named("auth"))) }
    single(named("no_auth")) { getRetrofit(get(named("no_auth"))) }

    single { get<Retrofit>().create(ColorService::class.java) }
    single { get<Retrofit>().create(PatternService::class.java) }
    // example no put authorization into header request
    //single { get<Retrofit>(named("no_auth")).create(liv3lyServices::class.java) }
}

fun getRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(NullOrEmptyConverterFactory().converterFactory())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
}

fun getOkHttpClientBuilder(
    authenticatorInterceptor: AuthenticatorInterceptor,
    loggingInterceptor: HttpLoggingInterceptor,
    withAuth: Boolean
): OkHttpClient {

    val httpClient = OkHttpClient.Builder()
        .connectTimeout(AppConfig.TIMEOUT_CONNECT_MILLIS, TimeUnit.MILLISECONDS)
        .readTimeout(AppConfig.TIMEOUT_READ_MILLIS, TimeUnit.MILLISECONDS)
        .writeTimeout(AppConfig.TIMEOUT_WRITE_MILLIS, TimeUnit.MILLISECONDS)
    if (withAuth) {
        httpClient.addInterceptor(authenticatorInterceptor)
    }
    if (BuildConfig.DEBUG) {
        httpClient.addInterceptor(loggingInterceptor)
    }

    configSSL(httpClient)

    return httpClient.build()
}

private fun configSSL(httpClient: OkHttpClient.Builder) {
    try {
        val trustAllCerts =
            arrayOf<TrustManager>(object : X509TrustManager {

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

                @SuppressLint("TrustAllX509TrustManager")
                override fun checkServerTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }

                @SuppressLint("TrustAllX509TrustManager")
                override fun checkClientTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }
            })
        val trustManager = trustAllCerts[0] as X509TrustManager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, SecureRandom())
        httpClient.sslSocketFactory(sslContext.socketFactory, trustManager)
        val hostnameVerifier = HostnameVerifier { _: String?, _: SSLSession? -> true }
        httpClient.hostnameVerifier(hostnameVerifier)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun getLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
    level = HttpLoggingInterceptor.Level.BODY
}