package com.liv3ly.app.extensions

import com.liv3ly.app.BuildConfig
import timber.log.Timber

fun Exception.safeLog() {
    if (BuildConfig.DEBUG)
        printStackTrace()
}