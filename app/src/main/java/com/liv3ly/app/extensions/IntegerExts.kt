package com.liv3ly.app.extensions

import android.content.res.Resources


/**
 * Extension method to convert pixel to dpi
 */
fun Int.dp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

/**
 * Extension method to convert dpi to pixel
 */
fun Int.px(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()
