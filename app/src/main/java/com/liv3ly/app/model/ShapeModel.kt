package com.liv3ly.app.model

import android.graphics.Bitmap
import android.graphics.Point
import androidx.lifecycle.LiveData
import com.liv3ly.app.ui.screens.tabbar.TYPEDRAW
import java.util.*

data class ShapeModel(
    val x: Float,
    val y: Float,
    val width: Int,
    val height: Int,
    var color: Int,
    var bitmap: Bitmap?,
    val arrayPoint: ArrayList<Point>?=null,
    val iType: TYPEDRAW
)