package com.liv3ly.app.retrofit

import com.liv3ly.app.data.local.pref.PrefHelper
import okhttp3.Interceptor
import okhttp3.Response

class AuthenticatorInterceptor(private val prefHelper: PrefHelper) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val url = original.url().newBuilder().build()

        val requestBuilder = original.newBuilder().url(url)
        val token = prefHelper.getUserToken()
        if(!token.isNullOrEmpty()) {
            requestBuilder.addHeader("Authorization", token)
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}