package com.vatnow.app.retrofit

import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

object MultipartHelper {
    fun createRequestBodyFromFile(file: File): RequestBody {
        val extension = file.path.substring(file.path.lastIndexOf(".") + 1)
        val contentType = "image/$extension"

        return RequestBody.create(MediaType.parse(contentType), file)
    }
}