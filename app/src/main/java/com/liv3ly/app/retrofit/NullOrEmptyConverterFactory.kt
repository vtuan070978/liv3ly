package com.liv3ly.app.retrofit

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.internal.operators.observable.ObservableEmpty
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NullOrEmptyConverterFactory : Converter.Factory() {

    fun converterFactory() = this

    override fun responseBodyConverter(
        type: Type?,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, Any>? {
        return Converter { responseBody ->
            if (responseBody.contentLength() == 0L) {
                null
            } else {
                type?.let {
                    retrofit.nextResponseBodyConverter<Any>(this, it, annotations)
                        .convert(responseBody)
                }
            }
        }
    }
}

open class NetworkException(error: Throwable) : RuntimeException(error)

class NoNetworkException(error: Throwable) : NetworkException(error)

class ServerUnreachableException(error: Throwable) : NetworkException(error)

class HttpCallFailureException(response: Response<*>) : HttpException(response)

fun <T> Single<T>.mapNetworkErrors(): Single<T> =
    this.onErrorResumeNext { error ->
        when (error) {
            is SocketTimeoutException -> Single.error(NoNetworkException(error))
            is UnknownHostException -> Single.error(ServerUnreachableException(error))
            is HttpCallFailureException -> Single.error(HttpCallFailureException(error.response()!!))
            else -> Single.error(error)
        }
    }


/*
fun <T> Single<Response<BaseResponse<T>>>.mapETagToBaseResponse(): Single<BaseResponse<T>> =
    this.mapNetworkErrors()
        .flatMap {
            if (it.code() == HttpStatusCode.NOT_MODIFIED) {
                Single.just(BaseResponse(
                    statusCode = it.code()
                ))
            } else {
                val result = it.body()?.copy(
                    eTag = it.headers()[HttpHeader.HEADER_ETAG]
                )
                Single.just(result)
            }
        }

fun <T> Single<Response<BasePagingResponse<T>>>.mapETagToBasePagingResponse(): Single<BasePagingResponse<T>> =
    this.mapNetworkErrors()
        .flatMap {
            if (it.code() == HttpStatusCode.NOT_MODIFIED) {
                Single.just(BasePagingResponse(PaginateValues()))
            } else {
                val result = it.body()?.copy(
                    eTag = it.headers()[HttpHeader.HEADER_ETAG]
                )
                Single.just(result)
            }
        }
*/

fun <T> Single<T>.skipResult() =
    this.subscribe({}, {})
