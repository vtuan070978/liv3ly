package com.liv3ly.app.retrofit;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OrderQueryMap extends LinkedHashMap<String, Object> {
    public OrderQueryMap(Map<String, Object> m) {
        super(m);
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        Set<Entry<String, Object>> originSet = super.entrySet();
        Set<Entry<String, Object>> newSet = new LinkedHashSet<>();

        for (Entry<String, Object> entry : originSet) {
            String entryKey = entry.getKey();
            if (entryKey == null) {
                throw new IllegalArgumentException("Map contained null key.");
            }
            Object entryValue = entry.getValue();
            if (entryValue == null) {
                throw new IllegalArgumentException(
                        "Map contained null value for key '" + entryKey + "'.");
            } else if (entryValue instanceof List) {
                for (Object arrayValue : (List) entryValue) {
                    if (arrayValue != null) { // Skip null values
                        Entry<String, Object> newEntry = new SimpleEntry<>(entryKey, arrayValue);
                        newSet.add(newEntry);
                    }
                }
            } else {
                Entry<String, Object> newEntry = new SimpleEntry<>(entryKey, entryValue);
                newSet.add(newEntry);
            }
        }
        return newSet;
    }
}