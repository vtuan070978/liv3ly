package com.liv3ly.app.retrofit

import com.liv3ly.app.ui.utils.StringUtils
import org.koin.core.KoinComponent
import org.koin.core.inject
import retrofit2.HttpException
import java.io.IOException

object ResponseHandler : KoinComponent {
    private val stringUtils: StringUtils by inject()

    /*
    fun getError(throwable: Throwable): ErrorResponse {
        val defaultError = stringUtils.getString(R.string.err_unknown)
        return when (throwable) {
            is HttpException -> {
                when (throwable.code()) {
                    HttpStatusCode.UNAUTHORIZED -> {
                        ErrorResponse(
                            ErrorType.ERROR_UNAUTHORIZED,
                            throwable.code(),
                            getResponseMessage(throwable) ?: defaultError
                        )
                    }
                    HttpStatusCode.NOT_FOUND -> {
                        ErrorResponse(
                            ErrorType.ERROR_NOT_FOUND,
                            throwable.code(),
                            getResponseMessage(throwable) ?: defaultError
                        )
                    }
                    else -> {
                        var errorResponse = ErrorResponse(
                            ErrorType.ERROR_DEFAULT,
                            throwable.code(),
                            getResponseMessage(throwable) ?: defaultError
                        )
                        val body = throwable.response()?.errorBody()
                        body?.source()?.let {
                            val moshiAdapter = Moshi
                                .Builder()
                                .add(
                                    DefaultOnDataMismatchAdapter.newFactory(
                                        ErrorResponse::class.java,
                                        errorResponse
                                    )
                                )
                                .build()
                                .adapter(ErrorResponse::class.java)
                            errorResponse = moshiAdapter.fromJson(it)!!
                        }
                        errorResponse
                    }
                }
            }
            is IOException, is NetworkException -> {
                ErrorResponse(
                    type = ErrorType.ERROR_NETWORK,
                    error = stringUtils.getString(R.string.internet_not_working)
                )
            }
            else -> {
                ErrorResponse(ErrorType.ERROR_UNKNOWN, error = throwable.message ?: defaultError)
            }
        }
    }

    private fun getResponseMessage(exception: HttpException): String? {
        val message = exception.message()
        return if (!message.isNullOrEmpty()) {
            message
        } else {
            val jsonObject = exception.response()?.errorBody()?.string()?.trim()?.let {
                try {
                    JSONObject(it)
                } catch (e: Exception) {
                    null
                }
            }
            jsonObject?.run {
                if (has("error") && !isNull("error")) {
                    getString("error")
                } else {
                    ""
                }
            }
        }
    }*/
}