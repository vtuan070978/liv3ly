package com.liv3ly.app.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.liv3ly.app.extensions.hideSoftKeyboard

abstract class BaseActivity<B : ViewDataBinding, VM: BaseViewModel>(
    @LayoutRes
    private val layoutId: Int
) : AppCompatActivity() {

    protected abstract val viewModel: VM
    lateinit var viewBinding: B

    abstract fun setupView(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, layoutId)
        viewBinding.lifecycleOwner = this
        setupView(savedInstanceState)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onStop() {
        super.onStop()
        hideSoftKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    protected fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(this, Observer {
            it?.let(onChanged)
        })
    }
}