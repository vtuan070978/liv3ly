package com.liv3ly.app.ui.base

import androidx.lifecycle.ViewModel
import com.liv3ly.app.data.common.ErrorResponse
import com.liv3ly.app.ui.utils.StringUtils
import io.reactivex.functions.Consumer
import io.sellmair.disposer.Disposer
import org.greenrobot.eventbus.EventBus

abstract class BaseViewModel(protected val stringUtils: StringUtils) : ViewModel() {

    protected val disposer = Disposer.create()

    protected val onErrorConsumer = Consumer<Throwable> { throwable ->

        //val errorResponse = ResponseHandl.getError(throwable)
        handleError(ErrorResponse(message = throwable.message))

    }

    abstract fun handleError(errorResponse: ErrorResponse)

    override fun onCleared() {
        disposer.dispose()
        super.onCleared()
    }
}