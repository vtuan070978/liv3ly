package com.liv3ly.app.ui.screens

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.liv3ly.app.R
import com.liv3ly.app.databinding.ActivityMainBinding
import com.liv3ly.app.ui.base.BaseActivity
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(
        layoutId = R.layout.activity_main
){

    override val viewModel: MainViewModel by inject()
    override fun setupView(savedInstanceState: Bundle?) {
        val sectionsPagerAdapter = PageTabAdapter(this, supportFragmentManager)
        viewBinding.viewPager.adapter = sectionsPagerAdapter
        viewBinding.tabs.setupWithViewPager(viewBinding.viewPager)
    }


}
