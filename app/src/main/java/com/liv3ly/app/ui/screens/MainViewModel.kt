package com.liv3ly.app.ui.screens

import com.liv3ly.app.data.common.ErrorResponse
import com.liv3ly.app.ui.base.BaseViewModel
import com.liv3ly.app.ui.utils.StringUtils

class MainViewModel(
    stringUtils: StringUtils
) : BaseViewModel(stringUtils) {


    override fun handleError(errorResponse: ErrorResponse) {
    }
}