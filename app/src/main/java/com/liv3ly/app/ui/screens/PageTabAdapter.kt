package com.liv3ly.app.ui.screens

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.liv3ly.app.R
import com.liv3ly.app.ui.screens.tabbar.ShapeFragment
import com.liv3ly.app.ui.screens.tabbar.TYPEDRAW

class PageTabAdapter(private val mContext: Context, fm: FragmentManager?) : FragmentPagerAdapter(fm!!, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return when (position) {
            0 -> {
                ShapeFragment.newInstance(position + 1, TYPEDRAW.SQUARES)
            }
            1 -> {
                ShapeFragment.newInstance(position + 1, TYPEDRAW.CIRCLES)
            }
            2 -> {
                ShapeFragment.newInstance(position + 1, TYPEDRAW.TRIANGLES)
            }
            else -> {
                ShapeFragment.newInstance(position + 1, TYPEDRAW.ALL)
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mContext.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return 4
    }

    companion object {
        @StringRes
        private val TAB_TITLES = intArrayOf(R.string.tab_text_squares, R.string.tab_text_circles, R.string.tab_text_triangles, R.string.tab_text_all)
    }

}