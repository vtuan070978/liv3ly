package com.liv3ly.app.ui.screens.splash

import android.os.Bundle
import com.liv3ly.app.R
import com.liv3ly.app.databinding.ActivitySplashBinding
import com.liv3ly.app.extensions.launchActivity
import com.liv3ly.app.ui.base.BaseActivity
import com.liv3ly.app.ui.screens.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SplashScreenActivity: BaseActivity<ActivitySplashBinding, SplashScreenViewModel>(
    layoutId = R.layout.activity_splash
)  {

    override val viewModel: SplashScreenViewModel by inject()
    private val activityScope = CoroutineScope(Dispatchers.Main)

    override fun setupView(savedInstanceState: Bundle?) {
        activityScope.launch {
            delay(1000)
            launchActivity<MainActivity> {  }
            this@SplashScreenActivity.finish()
        }
    }
    /*@SuppressLint("CheckResult")
    private fun getDeviceAdvertisingId(context: Context) {
        Observable.fromCallable {
            AdvertisingIdClient.getAdvertisingIdInfo(context).id
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ uuid ->
                appPrefs.saveDeviceID(uuid)
            }, { throwable ->
                Timber.d(throwable)
            })
    }*/
}

