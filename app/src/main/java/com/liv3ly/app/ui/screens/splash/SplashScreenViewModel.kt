package com.liv3ly.app.ui.screens.splash

import com.liv3ly.app.data.common.ErrorResponse
import com.liv3ly.app.data.local.pref.PrefHelper
import com.liv3ly.app.ui.base.BaseViewModel
import com.liv3ly.app.ui.utils.StringUtils

class SplashScreenViewModel(
    private val appPrefs: PrefHelper,
    stringUtils: StringUtils
) : BaseViewModel(stringUtils){

    override fun handleError(errorResponse: ErrorResponse) {
    }
}