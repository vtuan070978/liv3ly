package com.liv3ly.app.ui.screens.tabbar

import android.R.attr
import android.content.Context
import android.graphics.*
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.core.view.GestureDetectorCompat
import com.liv3ly.app.R
import com.liv3ly.app.model.ShapeModel

class Li3LyShapeView(context: Context?, val shapeModel: ShapeModel?, private val doubleTapCallback: DoubleTapCallback) : View(context),
    GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener {
    private lateinit var mDetector: GestureDetectorCompat
    private val mPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        when {
            shapeModel?.iType?.name.equals(TYPEDRAW.SQUARES.name) -> {
                drawSquares(canvas)
            }
            shapeModel?.iType?.name.equals(TYPEDRAW.TRIANGLES.name) -> {
                drawTriAngles(canvas)
            }
            else -> {
                drawCircle(canvas)
            }
        }

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (mDetector.onTouchEvent(event)) {
            true
        } else {
            super.onTouchEvent(event)
        }
    }

    private fun drawCircle(canvas: Canvas) {
        mPaint.color = shapeModel?.color!!
        mPaint.style = Paint.Style.FILL
        canvas.drawCircle(
            shapeModel.width.toFloat() / 2,
            shapeModel.width.toFloat() / 2,
            shapeModel.width.toFloat() / 2,
            mPaint
        )
    }

    private fun drawSquares(canvas: Canvas) {
        if (shapeModel?.bitmap == null) {
            mPaint.style = Paint.Style.FILL
            mPaint.color = shapeModel?.color!!
            canvas.drawRect(
                0f, 0f, shapeModel.x + shapeModel.width,
                shapeModel.y + shapeModel.height, mPaint
            )
        } else {
            drawSquaresBitmap(canvas, shapeModel.bitmap!!)
        }
    }

    private fun drawSquaresBitmap(canvas: Canvas, bitmap: Bitmap) {
        val srcRect = Rect(0,0, bitmap.width, bitmap.height)
        val desRect = Rect(0,0, shapeModel?.width!!, shapeModel.height)
        canvas.drawBitmap(bitmap, srcRect, desRect, mPaint)
    }

    private fun drawTriAngles(canvas: Canvas) {
        if (shapeModel?.arrayPoint == null || shapeModel.arrayPoint.size != 3) {
            return
        }
        val path = createPath()
        if (shapeModel.bitmap == null) {
            mPaint.color = shapeModel.color
            mPaint.style = Paint.Style.FILL
            canvas.drawPath(path, mPaint)
        } else {
            val shapeBitmap = shapeModel.bitmap!!
            val srcRect = Rect(0, 0, shapeBitmap.width, shapeBitmap.height)
            val destRect = Rect(0, 0, width, height)
            canvas.clipPath(path)
            canvas.drawBitmap(shapeBitmap, srcRect, destRect, mPaint)
        }
        path.close()
    }

    private fun createPath(): Path {
        val path = Path()
        val point1 = shapeModel?.arrayPoint?.get(0)!!
        val point2 = shapeModel.arrayPoint[1]
        val point3 = shapeModel.arrayPoint[2]
        path.moveTo(point1.x.toFloat(), point1.y.toFloat())
        path.lineTo(point2.x.toFloat(), point2.y.toFloat())
        path.lineTo(point3.x.toFloat(), point3.y.toFloat())
        path.lineTo(point1.x.toFloat(), point1.y.toFloat())
        return path
    }

    fun init() {
        mDetector = GestureDetectorCompat(context, this)
        mDetector.setOnDoubleTapListener(this)
    }

    override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
        if (e?.action == MotionEvent.ACTION_DOWN) {
            return true
        }
        return false
    }

    override fun onDoubleTap(e: MotionEvent?): Boolean {
        doubleTapCallback.onDoubleClick(this)
        return false
    }

    override fun onDoubleTapEvent(e: MotionEvent?): Boolean {
        return true
    }

    interface DoubleTapCallback {
        fun onDoubleClick(v: View?)
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return true
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return true
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return true
    }

    override fun onLongPress(e: MotionEvent?) {

    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return true
    }
}