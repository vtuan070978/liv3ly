package com.liv3ly.app.ui.screens.tabbar

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import androidx.core.view.GestureDetectorCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.liv3ly.app.R
import com.liv3ly.app.constants.key.ExtraKeys
import com.liv3ly.app.databinding.FragmentShapeBinding
import com.liv3ly.app.event.OnClearShapesEvent
import com.liv3ly.app.model.ShapeModel
import com.liv3ly.app.ui.base.BaseFragment
import com.liv3ly.app.utility.Utility
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.sqrt


class ShapeFragment: BaseFragment<FragmentShapeBinding, ShapeViewModel>(
        layoutId = R.layout.fragment_shape
), Li3LyShapeView.DoubleTapCallback {
    var iShapeChild = TYPEDRAW.CIRCLES
    var iShapeChildBg = TYPEDRAW.CIRCLES
    override val viewModel: ShapeViewModel by viewModel()
    var posisionX: Float = 0f
    var positionY: Float = 0f
    var li3LyShapeWxistView: Li3LyShapeView? = null
    private lateinit var mDetector: GestureDetectorCompat

    private var sensorManager: SensorManager? = null
    private var acceleration = 0f
    private var currentAcceleration = 0f
    private var lastAcceleration = 0f

    companion object {
        fun newInstance(tabIndex: Int, inputType: TYPEDRAW): ShapeFragment{
            val args = Bundle()

            val fragment = ShapeFragment()
            fragment.arguments = args
            args.putInt(ExtraKeys.EXTRA_TAB_INDEX, tabIndex)
            args.putInt(ExtraKeys.INDEX_TYPEDRAW, inputType.ordinal)
            return fragment
        }
    }

    override fun onInitDataBinding() {
        EventBus.getDefault().register(this)
        viewBinding.viewModel = viewModel
        viewModel.setTabIndex(arguments?.getInt(ExtraKeys.EXTRA_TAB_INDEX) ?: 0)
        viewModel.setTypeDraw(TYPEDRAW.values()[arguments?.getInt(ExtraKeys.INDEX_TYPEDRAW) ?: 0])
        observe(viewModel.state, ::onViewStateChange)
        observe(viewModel.event, ::onViewEventChange)
        initSendor()
    }

    private fun initSendor() {
        sensorManager = requireActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        Objects.requireNonNull(sensorManager)!!.registerListener(sensorListener, sensorManager!!
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        acceleration = 10f
        currentAcceleration = SensorManager.GRAVITY_EARTH
        lastAcceleration = SensorManager.GRAVITY_EARTH
    }

    private val sensorListener: SensorEventListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent) {
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]
            lastAcceleration = currentAcceleration
            currentAcceleration = sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta: Float = currentAcceleration - lastAcceleration
            acceleration = acceleration * 0.9f + delta
            if (acceleration > 12) {
                Timber.d("acceleration: %s", acceleration.toString())
                EventBus.getDefault().postSticky(OnClearShapesEvent())
            }
        }
        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    }

    private fun onViewStateChange(viewState: ShapeViewState) {
        when (viewState) {
            is ShapeViewState.Loaded -> {
                when (viewModel.typeDraw.value) {
                    TYPEDRAW.CIRCLES -> {
                        addCircles(posisionX, positionY, viewModel.colorData.value?.hex)
                    }
                    TYPEDRAW.SQUARES -> {
                        loadImage(viewModel.patternData.value?.imageUrl!!, TYPEDRAW.SQUARES)
                    }
                    TYPEDRAW.TRIANGLES -> {
                        processTriangle(true)
                    }
                    else -> { //ALL
                        when (iShapeChild) {
                            TYPEDRAW.CIRCLES -> {
                                addCircles(posisionX, positionY, viewModel.colorData.value?.hex)
                            }
                            TYPEDRAW.SQUARES -> {
                                loadImage(viewModel.patternData.value?.imageUrl!!, TYPEDRAW.SQUARES)
                            }
                            TYPEDRAW.TRIANGLES -> {
                                processTriangle(true)
                            }
                            else -> {
                            }
                        }
                    }
                }
            }
            is ShapeViewState.OnFailure -> {
                when {
                    viewModel.typeDraw.value?.name.equals(TYPEDRAW.CIRCLES.name) -> {
                        addCircles(posisionX, positionY, "")
                    }
                    viewModel.typeDraw.value?.name.equals(TYPEDRAW.SQUARES.name) -> {
                        addSquareView(posisionX, positionY, null)
                    }
                    viewModel.typeDraw.value?.name.equals(TYPEDRAW.TRIANGLES.name) -> {
                        processTriangle(false)
                    }
                    else -> { //ALL
                        when (iShapeChild) {
                            TYPEDRAW.CIRCLES -> {
                                addCircles(posisionX, positionY, "")
                            }
                            TYPEDRAW.SQUARES -> {
                                addSquareView(posisionX, positionY, null)
                            }
                            TYPEDRAW.TRIANGLES -> {
                                processTriangle(false)
                            }
                            else -> {
                            }
                        }
                    }
                }
            }
            else -> { }
        }
    }

    private fun processTriangle(bState: Boolean) {
        when (iShapeChildBg) {
            TYPEDRAW.CIRCLES -> {
                addTrianglesView(posisionX, positionY, if (bState && viewModel.colorData.value != null) viewModel.colorData.value?.hex!! else "", null)
            }
            TYPEDRAW.SQUARES -> {
                if (bState) {
                    loadImage(viewModel.patternData.value?.imageUrl!!, TYPEDRAW.TRIANGLES)
                } else {
                    addTrianglesView(posisionX, positionY, "", null)
                }
            }
            else -> {
                addTrianglesView(posisionX, positionY, null, null)
            }
        }
    }

    private fun onViewEventChange(viewEvent: ShapeViewEvent) {
        if (viewEvent is ShapeViewEvent.ShapeOnTouchListener) {
            if (viewEvent.motionEvent.action == MotionEvent.ACTION_DOWN) {
                val pointerId: Int = viewEvent.motionEvent.getPointerId(0)
                val pointerIndex: Int = viewEvent.motionEvent.findPointerIndex(pointerId)
                // Get the pointer's current position
                posisionX = viewEvent.motionEvent.getX(pointerIndex)
                positionY = viewEvent.motionEvent.getY(pointerIndex)
                // Call API
                li3LyShapeWxistView = null
                processGetApi(viewModel.typeDraw.value!!)
            }
        }
    }

    private fun processGetApi(iTypeDraw: TYPEDRAW) {
        when (iTypeDraw) {
            TYPEDRAW.CIRCLES -> {
                viewModel.getColourFromCloud()
            }
            TYPEDRAW.SQUARES -> {
                viewModel.getPatternFromCloud()
            }
            TYPEDRAW.TRIANGLES -> {
                iShapeChildBg = randomTypeTriAngle()
                if (iShapeChildBg == TYPEDRAW.CIRCLES) {
                    viewModel.getColourFromCloud()
                } else {
                    viewModel.getPatternFromCloud()
                }
            }
            else -> {
                iShapeChildBg = randomTypeAll()
                if (iShapeChildBg == TYPEDRAW.CIRCLES) {
                    viewModel.getColourFromCloud()
                } else {
                    viewModel.getPatternFromCloud()
                }
            }
        }

    }

    private fun randomTypeTriAngle(): TYPEDRAW {
        val iRandom = Random().nextInt(2)
        return TYPEDRAW.values()[iRandom]
    }

    private fun randomTypeAll(): TYPEDRAW {
        val iRandom = Random().nextInt(TYPEDRAW.values().size - 1)
        iShapeChild = TYPEDRAW.values()[iRandom]
        return if (iShapeChild == TYPEDRAW.TRIANGLES) {
            randomTypeTriAngle()
        } else {
            TYPEDRAW.values()[iRandom]
        }
    }

    private fun parseHexColor(hexColor: String?): Int {
        var iColor = Utility.getRandomColor(requireActivity())
        if (!hexColor.isNullOrEmpty()) {
            try {
                var hexColorTemp = hexColor
                if (!hexColor.startsWith("#")) {
                    hexColorTemp = "#$hexColor"
                }
                iColor = Color.parseColor(hexColorTemp)
            } catch (ex: Exception) {
                Timber.e(ex)
            }
        }
        return iColor
    }

    private fun loadImage(urlImage: String, typeShape: TYPEDRAW) {
        Handler().post {
            loadImageUrl(urlImage, typeShape)
        }
    }
    // load bitmap from url
    private fun loadImageUrl(urlImage: String, typeShape: TYPEDRAW) {
        Glide.with(requireActivity()).asBitmap().load(urlImage)
            .centerCrop()
            .into(object : CustomTarget<Bitmap?>() {
                override fun onResourceReady(
                        bitmap: Bitmap,
                        transition: Transition<in Bitmap?>?
                ) {
                    Timber.d("onResourceReady")
                    if (typeShape.name == TYPEDRAW.SQUARES.name) {
                        addSquareView(posisionX, positionY, bitmap)
                    } else {
                        addTrianglesView(posisionX, positionY, "", bitmap)
                    }
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    Timber.d("onLoadCleared")
                }

                override fun onStop() {
                    super.onStop()
                    Timber.d("onStop")
                }
            })
    }

    // add circle
    private fun addCircles(floatX: Float, floatY: Float, hexColor: String?) {
        var iColor = parseHexColor(hexColor)

        if (li3LyShapeWxistView == null) {
            val width = Utility.getWidthRandom(requireActivity())
            val objectModel = ShapeModel(
                    floatX, floatY,
                    width,
                    width,
                    iColor,
                    null,
                    null,
                    TYPEDRAW.CIRCLES
            )

            val squaresView = Li3LyShapeView(activity, objectModel, this)
            squaresView.init()
            val layoutParams = FrameLayout.LayoutParams(objectModel.width, objectModel.width)
            layoutParams.setMargins(
                    objectModel.x.toInt() - objectModel.width / 2,
                    objectModel.y.toInt() - objectModel.width / 2,
                    0,
                    0
            )
            squaresView.layoutParams = layoutParams
            viewBinding.shapesFrameLayout.addView(squaresView)
            viewBinding.executePendingBindings()
            applyAnimation(squaresView)
        } else {
            li3LyShapeWxistView?.shapeModel?.color = iColor
            li3LyShapeWxistView?.invalidate()
        }
    }

    // add SquareView
    private fun addSquareView(floatX: Float, floatY: Float, bitmap: Bitmap?) {
        if (li3LyShapeWxistView == null) {
            val width = Utility.getWidthRandom(requireActivity())
            val objectModel = ShapeModel(
                    floatX, floatY,
                    width,
                    width,
                    Utility.getRandomColor(requireActivity()),
                    bitmap,
                    null,
                    TYPEDRAW.SQUARES
            )
            val squaresView = Li3LyShapeView(activity, objectModel, this)
            squaresView.init()
            val layoutParams = FrameLayout.LayoutParams(objectModel.width, objectModel.height)
            layoutParams.setMargins(
                    objectModel.x.toInt() - objectModel.width / 2,
                    objectModel.y.toInt() - objectModel.height / 2,
                    0,
                    0
            )
            squaresView.layoutParams = layoutParams
            viewBinding.shapesFrameLayout.addView(squaresView)
            viewBinding.executePendingBindings()
            applyAnimation(squaresView)
        } else {
            li3LyShapeWxistView?.shapeModel?.color = Utility.getRandomColor(requireActivity())
            li3LyShapeWxistView?.shapeModel?.bitmap = bitmap
            li3LyShapeWxistView?.invalidate()
        }
    }

    // add Triangle view
    private fun addTrianglesView(floatX: Float, floatY: Float, hexColor: String?, bitmap: Bitmap?) {
        if (li3LyShapeWxistView == null) {
            val width = Utility.getWidthRandom(requireActivity())
            val height = width
            val point1 = Point(width / 2, 0)
            val point2 = Point(0, height)
            val point3 = Point(width, height)

            var arrayPoint = ArrayList<Point>()
            arrayPoint.add(point1)
            arrayPoint.add(point2)
            arrayPoint.add(point3)
            val objectModel = ShapeModel(
                    posisionX, positionY,
                    width,
                    height,
                    parseHexColor(hexColor),
                    bitmap,
                    arrayPoint,
                    TYPEDRAW.TRIANGLES
            )
            val squaresView = Li3LyShapeView(activity, objectModel, this)
            squaresView.init()
            val layoutParams = FrameLayout.LayoutParams(width, height)
            layoutParams.setMargins(floatX.toInt() - width / 2, floatY.toInt() - height / 2, 0, 0)
            squaresView.layoutParams = layoutParams
            viewBinding.shapesFrameLayout.addView(squaresView)
            viewBinding.executePendingBindings()
            applyAnimation(squaresView)
        } else {
            li3LyShapeWxistView?.shapeModel?.color = parseHexColor(hexColor)
            li3LyShapeWxistView?.shapeModel?.bitmap = bitmap
            li3LyShapeWxistView?.invalidate()
        }
    }

    private fun applyAnimation(shapeView: Li3LyShapeView) {
        val anim = AnimationUtils.loadAnimation(context, R.anim.zoom_out)
        shapeView.startAnimation(anim)
    }

    override fun onDoubleClick(v: View?) {
        if (v != null) {
            li3LyShapeWxistView = v as Li3LyShapeView?
            if (viewModel.typeDraw.value == TYPEDRAW.ALL) {
                iShapeChild = li3LyShapeWxistView?.shapeModel?.iType!!
            }
            processGetApi(li3LyShapeWxistView?.shapeModel?.iType!!)
        }
    }

    override fun onResume() {
        sensorManager?.registerListener(sensorListener, sensorManager!!.getDefaultSensor(
                Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL
        )
        super.onResume()
    }
    override fun onPause() {
        sensorManager!!.unregisterListener(sensorListener)
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onClearShapeEvent(event: OnClearShapesEvent?) {
        try {
            viewBinding.shapesFrameLayout.removeAllViews()
            viewBinding.shapesFrameLayout.removeAllViewsInLayout()
        } catch (ex: Exception) {
            Timber.e(ex)
        }
    }
}