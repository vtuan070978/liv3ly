package com.liv3ly.app.ui.screens.tabbar

import android.view.MotionEvent
import android.view.View
import com.liv3ly.app.data.repositories.color.model.ColorModel
import com.liv3ly.app.data.common.ErrorResponse

sealed class ShapeViewEvent {
    data class ShapeOnTouchListener(val motionEvent: MotionEvent): ShapeViewEvent()
}