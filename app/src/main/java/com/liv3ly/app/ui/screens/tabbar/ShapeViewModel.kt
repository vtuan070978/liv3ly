package com.liv3ly.app.ui.screens.tabbar

import android.annotation.SuppressLint
import android.view.View
import android.view.View.OnTouchListener
import android.widget.FrameLayout
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.liv3ly.app.data.common.ErrorResponse
import com.liv3ly.app.data.repositories.color.ColorRepository
import com.liv3ly.app.data.repositories.color.model.ColorModel
import com.liv3ly.app.data.repositories.pattern.PatternRepository
import com.liv3ly.app.data.repositories.pattern.model.PatternModel
import com.liv3ly.app.retrofit.mapNetworkErrors
import com.liv3ly.app.ui.base.BaseViewModel
import com.liv3ly.app.ui.utils.SingleLiveEvent
import com.liv3ly.app.ui.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.sellmair.disposer.disposeBy

enum class TYPEDRAW {
    SQUARES,
    CIRCLES,
    TRIANGLES,
    ALL
}

class ShapeViewModel(
    private val colorRepository: ColorRepository,
    private val patternRepository: PatternRepository,
    stringUtils: StringUtils
) : BaseViewModel(stringUtils) {

    private val _tabIndex = SingleLiveEvent<Int>()
    private val tabIndex: LiveData<Int>
        get() = _tabIndex

    private val _typeDraw = SingleLiveEvent<TYPEDRAW>()
    val typeDraw: LiveData<TYPEDRAW>
        get() = _typeDraw


    private var _colourData = SingleLiveEvent<ColorModel>()
    val colorData: LiveData<ColorModel>
        get() = _colourData

    private var _patternData = SingleLiveEvent<PatternModel>()
    val patternData: LiveData<PatternModel>
        get() = _patternData

    private val _state = SingleLiveEvent<ShapeViewState>()
    val state: LiveData<ShapeViewState>
        get() = _state

    val loading: LiveData<Boolean> = Transformations.map(_state) {
        it == ShapeViewState.Loading
    }

    private val _event = SingleLiveEvent<ShapeViewEvent>()
    val event: LiveData<ShapeViewEvent>
        get() = _event

    fun setTabIndex(tab: Int) {
        _tabIndex.value = tab
        when (tabIndex.value) {
        }
    }

    fun setTypeDraw(inputTypeDraw: TYPEDRAW) {
        _typeDraw.value = inputTypeDraw

    }

    private fun createSquaresShape() {

    }

    private fun createCirclesShape() {

    }

    private fun createTrianglesShape() {

    }

    private fun createAllShapes() {
        createSquaresShape()
        createCirclesShape()
        createTrianglesShape()
    }

    fun getColourFromCloud() {
        _state.value = ShapeViewState.Loading
        val onSuccessConsumer = Consumer<List<ColorModel>> {
            // success
            _colourData.value = it[0]
            _state.value = ShapeViewState.Loaded
        }
        getColorObservable()
            .subscribe(onSuccessConsumer, onErrorConsumer)
            .disposeBy(disposer)
    }

    fun getPatternFromCloud() {
        _state.value = ShapeViewState.Loading
        val onSuccessConsumer = Consumer<List<PatternModel>> {
            // success
            _patternData.value = it[0]
            _state.value = ShapeViewState.Loaded
        }
        getPatternObservable()
            .subscribe(onSuccessConsumer, onErrorConsumer)
            .disposeBy(disposer)
    }

    private fun getColorObservable(): Single<List<ColorModel>> {
        return colorRepository.getColour().mapNetworkErrors()
            .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
    }

    private fun getPatternObservable(): Single<List<PatternModel>> {
        return patternRepository.getPatterns().mapNetworkErrors()
            .observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
    }

    private fun mergeColorAndPatternFromCloud() {
        _state.value = ShapeViewState.Loading
        Observable.merge(getColorObservable().toObservable(), getPatternObservable().toObservable())
            .subscribe(
                {
                    // onNext
                    if (it.asListOfType<ColorModel>() is List<ColorModel>) {
                        _colourData.value = (it as List<ColorModel>)[0]
                    } else if (it.asListOfType<PatternModel>() is List<PatternModel>) {
                        _patternData.value = (it as List<PatternModel>)[0]
                    }
                }, {
                    // onError
                    onErrorConsumer.accept(it)
                }, {
                    // onCompleted
                    _state.value = ShapeViewState.Loaded
                }
            ).disposeBy(disposer)
    }

    inline fun <reified T> List<*>.asListOfType(): List<T>? =
        if (all { it is T })
            @Suppress("UNCHECKED_CAST")
            this as List<T> else
            null

    override fun handleError(error: ErrorResponse) {
        _state.value = ShapeViewState.OnFailure(error)
    }

    // event form user interfaces
    @SuppressLint("ClickableViewAccessibility")
    fun getOnTouchListener(): View.OnTouchListener {
        return OnTouchListener { _, motionEvent ->
            if (loading.value == null || loading.value == false) {
                _event.value = ShapeViewEvent.ShapeOnTouchListener(motionEvent)
            }
            true
        }
    }

}