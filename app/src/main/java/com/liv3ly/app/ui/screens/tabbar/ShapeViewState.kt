package com.liv3ly.app.ui.screens.tabbar

import com.liv3ly.app.data.repositories.color.model.ColorModel
import com.liv3ly.app.data.common.ErrorResponse

sealed class ShapeViewState {
    object Loading : ShapeViewState()
    object Loaded : ShapeViewState()
    data class OnFailure(val error: ErrorResponse): ShapeViewState()
}