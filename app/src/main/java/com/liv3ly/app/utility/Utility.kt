package com.liv3ly.app.utility

import android.content.Context
import android.content.res.Resources
import com.liv3ly.app.R
import java.util.*

object Utility {

    fun getRandomColor(mContext: Context): Int {
        val androidColors = mContext.resources.getIntArray(R.array.android_colors)
        return androidColors[Random().nextInt(androidColors.size)]
    }

    fun getWidthRandom(context: Context): Int {
        val with = context.resources.displayMetrics.widthPixels
        val min: Int = with / 10
        val max: Int = with * 45 / 100
        val rand = Random()
        return rand.nextInt(max - min + 1) + min
    }

    fun getHeightRandom(context: Context): Int {
        val height = context.resources.displayMetrics.heightPixels
        val min: Int = height / 10
        val max: Int = height * 45 / 100
        val rand = Random()
        return rand.nextInt(max - min + 1) + min
    }
}
